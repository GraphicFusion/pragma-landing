<?php
	/**
	 * string	$args['rows']
	 * string	$args['rows'][0]['image']
	 * string	$args['rows'][0]['title']
	 * string	$args['rows'][0]['text']
	 * string	$args['rows'][0]['button_text']
	 * string	$args['rows'][0]['button_link']
	 */
	global $args;
?>
<div class="two-col-container">
	<?php foreach( $args['cols'] as $row ) : ?>
			<?php 
				$image_content = "<div class='alt-block-image'><img src='".$row['image']['url']."'></div>";
				$text_content = "
					<div class='alt-content-section wow '>
						<div class=''>
							<h3>".$row['title']."</h3>
							<div class='text'>".$row['text']."</div>";
							if( $row['button']  ){
								$text_content .= "
										<a href='". $row['button']['url'] ."' target='". $row['button']['target'] ."' class='btn'>
											". $row['button']['title'] ."
										</a>";
							}
							$text_content .= "
						</div><!--innersection--->
					</div><!--alt-content-section--->";
			?>
			<div class="twocolumn-section ">
					<?php echo $image_content.$text_content; ?>	
		  </div><!--/twocolumn-section--->

	<?php endforeach; ?>
</div>
