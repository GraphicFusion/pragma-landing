<?php
	function build_twocolumn_layout(){
			$rows = [];
				$rows[] = array(
					'title' => mason_get_sub_field('twocolumn_block_title_one'),
					'text' => mason_get_sub_field('twocolumn_block_text_one'),
					'image' => mason_get_sub_field('twocolumn_block_image_one'),
					'button' => mason_get_sub_field('twocolumn_block_button_one'),
				);
				$rows[] = array(
					'title' => mason_get_sub_field('twocolumn_block_title_two'),
					'text' => mason_get_sub_field('twocolumn_block_text_two'),
					'image' => mason_get_sub_field('twocolumn_block_image_two'),
					'button' => mason_get_sub_field('twocolumn_block_button_two'),
				);
			$args['cols'] = $rows;
		$args['module_styles'] = [];
		if(get_sub_field('twocolumn_block_padding-top')){
			$args['module_styles']['padding-top'] = get_sub_field('twocolumn_block_padding-top');
		}
		if(get_sub_field('twocolumn_block_padding-bottom')){
			$args['module_styles']['padding-bottom'] = get_sub_field('twocolumn_block_padding-bottom');
		}


		return $args;
	}

?>