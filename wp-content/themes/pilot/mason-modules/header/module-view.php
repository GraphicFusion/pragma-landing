<?php 
	/**
	 * string	$args['title']
	 * string	$args['content']
	 */
	global $args;
	global $wp;
	$current_url = home_url( add_query_arg( array(), $wp->request ) );
?>
<div class="header header-one">
	<div class="header-content interior-container">
					<div class="logo-wrapper">
						<a href="/"><img class="logo light-bg" src="<?php echo $args['light_bg_logo']['url']; ?>"></a></div>



		<div class="links">
		<?php if( count($args['main_links'])>0): ?>
			<ul class="primary-nav">
				<?php foreach($args['main_links'] as $main): 
					$main_href = $main['header_block_main_link']['url'];
					$active_class = "";
					if( rtrim($current_url,'/') == rtrim($main_href,'/') ){
						$active_class = " active";
					}
				?>
					<?php $subs = ""; if(1!=1 && is_array($main['header_block_sublinks']) && count($main['header_block_sublinks'])>0): ?>
						<?php foreach($main['header_block_sublinks'] as $sub ) : 
							if( isset($sub['header_block_sub_link']['url'])){
								$sub_href = $sub['header_block_sub_link']['url'];
								if( rtrim($current_url,'/') == rtrim($sub_href,'/') ){
									$active_class = " active";
								}
								$subs .= '<li><a href="'.$sub_href.'">'.$sub['header_block_sub_link']['title'].'</a></li>';
							}
						endforeach; ?>
					<?php endif; ?>
					<li class="<?php echo $active_class; ?>"><a href="<?php echo $main_href; ?>">
							<?php echo $main['header_block_main_link']['title']; ?>
						</a>
						<?php if($subs) : ?>
							<ul class="sub-nav">
								<?php echo $subs; ?>
							</ul>
						<?php endif; ?>
					</li>
				<?php endforeach; ?>
			</ul>
		<?php endif; ?>
	</div>
<div class="mobile-nav">
			<div class="menu-button">
				<img class="menu-open light-bg" src="<?php echo get_template_directory_uri(); ?>/mason-modules/header/Light-BG-Icon-Menu.svg">
				<img class="menu-close" src="<?php echo get_template_directory_uri(); ?>/mason-modules/header/Icon-Close.svg">
		</div>

		</div>

	</div>
</div>
<div class="header header-two">
	<div class="header-content interior-container">
		<div class="mobile-nav">
			<div class="menu-button">
				<img class="menu-close" src="<?php echo get_template_directory_uri(); ?>/mason-modules/header/Icon-Close.svg">
			</div>
		</div>
		<div class="links">

		</div>
	</div>
	<div class="mobile-menu content">
		<?php if( count($args['main_links'])>0): ?>
			<ul class="primary-nav">
				<?php foreach($args['main_links'] as $main): 
					$main_href = $main['header_block_main_link']['url'];
					$active_class = "";
					if( rtrim($current_url,'/') == rtrim($main_href,'/') ){
						$active_class = " active";
					}
				?>
					<?php $subs = ""; if(1!=1 && is_array($main['header_block_sublinks']) && count($main['header_block_sublinks'])>0): ?>
						<?php foreach($main['header_block_sublinks'] as $sub ) : 
							if( isset($sub['header_block_sub_link']['url'])){
								$sub_href = $sub['header_block_sub_link']['url'];
								if( rtrim($current_url,'/') == rtrim($sub_href,'/') ){
									$active_class = " active";
								}
								$subs .= '<li><a href="'.$sub_href.'">'.$sub['header_block_sub_link']['title'].'</a></li>';
							}
						endforeach; ?>
					<?php endif; ?>
					<li class="<?php echo $active_class; ?>"><a href="<?php echo $main_href; ?>">
							<?php echo $main['header_block_main_link']['title']; ?>
						</a>
						<?php if($subs) : ?>
							<ul class="sub-nav">
								<?php echo $subs; ?>
							</ul>
						<?php endif; ?>
					</li>
				<?php endforeach; ?>
			</ul>
		<?php endif; ?>
	</div>
</div>
<script>
	function getScrollBarWidth () {
	    var $outer = $('<div>').css({visibility: 'hidden', width: 100, overflow: 'scroll'}).appendTo('body'),
	        widthWithScroll = $('<div>').css({width: '100%'}).appendTo($outer).outerWidth();
	    $outer.remove();
	    return 100 - widthWithScroll;
	};
	$( document ).ready(function() {
		var sb = getScrollBarWidth();
		$('.menu-button').click(function(){
			if( $('.block-header').hasClass('open-mobile') ){
				$('.block-header').removeClass('open-mobile');
				$('body').removeClass('open-mobile');
			}
			else{
				$('.block-header').addClass('open-mobile');
				$('body').addClass('open-mobile');
			}
		});
	})
</script>