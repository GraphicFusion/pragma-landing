<?php
	function build_header_layout(){
		$args = array(
			'title' => get_sub_field('header_block_title'),
			'content' => mason_get_sub_field('header_block_content'),
			'dark_bg_logo' => mason_get_sub_field('header_block_dark_bg_logo'),
			'light_bg_logo' => mason_get_sub_field('header_block_light_bg_logo'),
			'main_links' => mason_get_sub_field('header_block_main_nav'),
		);
		$args['module_styles'] = [];
		if(get_sub_field('header_block_padding-top')){
			$args['module_styles']['padding-top'] = get_sub_field('header_block_padding-top');
		}
		if(get_sub_field('header_block_padding-bottom')){
			$args['module_styles']['padding-bottom'] = get_sub_field('header_block_padding-bottom');
		}
	

		return $args;
	}
?>