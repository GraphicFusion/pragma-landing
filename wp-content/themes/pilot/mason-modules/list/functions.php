<?php
	function build_list_layout(){
		$args = array(
			'title' => mason_get_sub_field('list_block_title'),
			'items' => mason_get_sub_field('list_block_items')
		);
		return $args;
	}
?>