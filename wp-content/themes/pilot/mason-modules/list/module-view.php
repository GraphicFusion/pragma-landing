<?php 
	/**
	 * string	$args['title']
	 * string	$args['content']
	 */
	global $args;
?>
<div class="interior-container interior-box-wide" >
<?php if($args['title']) : ?>
	<h2><?php echo $args['title']; ?></h2>
<?php endif; ?>
	<div class="list-wrap ">
		<?php foreach($args['items'] as $item): ?>
			<div class="list-item">
				<div class="blue-line"></div>
				<h3><?php echo $item['item']; ?></h3>
			</div>
		<?php endforeach; ?>
	</div>
</div>