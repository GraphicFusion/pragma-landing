<?php
	function build_slide_layout(){
		$rows_arr = mason_get_sub_field('slide_block_rows');
		$args = [];
		$args['isHeader'] = mason_get_sub_field('slide_block_isHeader');
		$height = mason_get_sub_field('slide_block_height');
		$args['height'] = ($height ? $height : 99);
		if( is_array($rows_arr) ):
			$rows = array();
			foreach( $rows_arr as $row_arr){
				$rows[] = array(
					'title' => $row_arr['slide_block_title'],
					'content' => $row_arr['slide_block_content'],
					'image' => $row_arr['slide_block_image'],
				);
				
			}
			while ( mason_have_rows('slide_block_rows') ) : the_row();
				$rows[] = array(
					'title' => get_sub_field('slide_block_title'),
					'content' => get_sub_field('slide_block_content'),
					'image' => get_sub_field('slide_block_image'),
				);
			endwhile;
			$args['rows'] = $rows;
		endif;
		$args['module_styles'] = [];
		if(get_sub_field('slide_block_padding-top')){
			$args['module_styles']['padding-top'] = get_sub_field('slide_block_padding-top');
		}
		if(get_sub_field('slide_block_padding-bottom')){
			$args['module_styles']['padding-bottom'] = get_sub_field('slide_block_padding-bottom');
		}


		return $args;
	}

?>