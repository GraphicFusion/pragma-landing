<?php
	/**
	 * string	$args['title']
	 * string	$args['content']
	 */
	global $args;
?>
<div class="footer">
	<div class="footer-content container">
		<div class="left">
				<?php if($args['logo']['url']) : ?>
					<img src="<?php echo $args['logo']['url']; ?>">
				<?php else: ?>
					<img src="/wp-content/themes/pilot/mason-modules/footer/Pragma-Logo-Stack-WHITE.svg">
				<?php endif; ?>
		</div>
				<div class="bottom-left">
					<div class="sub"><?php echo $args['subtitle']; ?></div>
					<div class="mail"><a target="_blank" href="mailto:<?php echo $args['email']; ?>"><?php echo $args['email']; ?></a></div>
				</div>
		<div class="right">
			<div class="links">
				<?php foreach($args['links'] as $link) : ?>
					<a href='<?php echo $link['footer_link']['url']; ?>'><h3><?php echo $link['footer_link']['title']; ?></h3></a>
				<?php endforeach; ?>
			</div>

		</div>
	</div>
</div>