<?php
	global $args;
	$block_class = "";
	$suffixes = [""];
	if($args['half_width'] ){
		$block_class = "half-width";
		$suffixes[] = "_second";
	}
?>
<div class="interior-container video-wrap" >
<?php
	foreach($suffixes as $suffix){
		$use_popup = 0;
		if( $args['use_popup'.$suffix] ){
			$use_popup = 1;
			$popup_video = $args['video_file_mp4'.$suffix];
			if( $args['youtube'.$suffix] ) {
				$popup_video = $args['youtube'.$suffix];
			}
		}
		$use_banner = 0;
		$use_bg_image = 0;
		$bg_image_url = "https://i.ytimg.com/vi/ID/hqdefault.jpg";
		if( (@$args['video_file_mp4'.$suffix] || $args['youtube'.$suffix] || $args['bg_image_url'.$suffix] ) ){
			$use_banner = 1;
			$use_bg_image = 1;
			$bg_image_url = $args['bg_image_url'.$suffix];
		}
		$use_title = 0;
		if( $args['title'.$suffix] || $args['content'.$suffix] ){
			$use_title = 1;
		}
		
	?>
		<div class="shadow-card <?php echo $block_class." ".$suffix; ?>">
			<div class="interior-container">
				<div class="youtube-player" data-id="<?php echo $args['bg_youtube'.$suffix]; ?>"></div>
			</div>						
				<div class="img-block-wrap" id="<?php echo $args['id'.$suffix]; ?>">
						<div class="bg-image" >
					<div class="video-content" style="z-index:3;">
						<div class="title-div">
							<div class="title-wrap">
								<?php if( $use_title && isset( $args['title'.$suffix] ) ) : ?>
									<h6><?php echo $args['title'.$suffix]; ?></h6>
								<?php endif; ?>
								<?php if(  $args['content'.$suffix]  ) : ?>
									<?php echo $args['content'.$suffix]; ?>
								<?php endif; ?>
							</div><!--/title-wrap-->
						</div><!--/title-->
					</div><!--/video-content-->
				</div><!--/bg-img-->
			</div><!--img-block-wrap-->
		</div>
	<?php } ?>
</div>
<script>
	

</script>
<style>
#html5-watermark{
	display:none !important;
}
</style>
<style>
	.interior-container{
		max-width:1200px;
		margin:0 auto;
	}
    .youtube-player {
        position: relative;
        padding-bottom: 56.23%;
        /* Use 75% for 4:3 videos */
        height: 0;
        overflow: hidden;
        max-width: 100%;
        background: #000;
    }
    
    .youtube-player iframe {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        z-index: 100;
        background: transparent;
    }
    
    .youtube-player img {
        bottom: 0;
        display: block;
        left: 0;
        margin: auto;
        max-width: 100%;
        width: 100%;
        position: absolute;
        right: 0;
        top: 0;
        border: none;
        height: auto;
        cursor: pointer;
        -webkit-transition: .4s all;
        -moz-transition: .4s all;
        transition: .4s all;
    }
    
    .youtube-player img:hover {
        -webkit-filter: brightness(75%);
    }
    
    .youtube-player .play {
        height: 50px;
        width: 50px;
        left: 50%;
        top: 50%;
        margin-left: -20px;
        margin-top: -20px;
        position: absolute;
        background: url("/wp-content/themes/pilot/mason-modules/video/img/play.svg") no-repeat;
        cursor: pointer;
    }

</style>
<script>

    document.addEventListener("DOMContentLoaded",
        function() {
            var div, n,
                v = document.getElementsByClassName("youtube-player");
            for (n = 0; n < v.length; n++) {
                div = document.createElement("div");
                div.setAttribute("data-id", v[n].dataset.id);
                div.innerHTML = labnolThumb(v[n].dataset.id);
                div.onclick = labnolIframe;
                v[n].appendChild(div);
            }
        });

    function labnolThumb(id) {
        var thumb = '<img src="<?php echo $bg_image_url; ?>">',
            play = '<div class="play"></div>';
        return thumb.replace("ID", id) + play;
    }

    function labnolIframe() {
        var iframe = document.createElement("iframe");
        var embed = "https://www.youtube.com/embed/ID?autoplay=1";
        iframe.setAttribute("src", embed.replace("ID", this.dataset.id));
        iframe.setAttribute("frameborder", "0");
        iframe.setAttribute("allowfullscreen", "1");
        iframe.setAttribute("allow", "autoplay");
        this.parentNode.replaceChild(iframe, this);
    }

</script>