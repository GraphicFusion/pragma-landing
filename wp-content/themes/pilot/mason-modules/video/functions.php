<?php
	function build_video_layout(){
		global $i;
		$half_width = mason_get_sub_field('video_block_two_half_widths');
		$suffixes = [""];
		if($half_width){
			$suffixes[] = "_second";
		}
		$args = [];
		foreach($suffixes as $suffix){
			$video_args = array(
				'half_width'.$suffix => $half_width,
				'subtitle'.$suffix => mason_get_sub_field('video_block_subtitle'.$suffix),
				'title'.$suffix => mason_get_sub_field('video_block_title'.$suffix),
				'button'.$suffix => mason_get_sub_field('video_block_link'.$suffix),
				'id'.$suffix => 'video_block'.$suffix.'_'.$i,
				'overlay_color'.$suffix => '',
				'overlay_opacity'.$suffix => '',
				'left_align_title'.$suffix => mason_get_sub_field('left_align_title'.$suffix),
				'content'.$suffix => mason_get_sub_field('video_block_content'.$suffix),
				'show_content'.$suffix => mason_get_sub_field('video_block_use_content'.$suffix),
				'use_banner'.$suffix => mason_get_sub_field('video_block_use_banner'.$suffix),
				'use_image'.$suffix => mason_get_sub_field('video_block_use_bg_image'.$suffix),
				'use_popup'.$suffix => mason_get_sub_field('video_block_use_popup'.$suffix),
				'use_overlay'.$suffix => mason_get_sub_field('video_block_modify'.$suffix)
			);
			if( mason_get_sub_field('video_block_modify'.$suffix) ){
				if( $opacity = mason_get_sub_field('video_block_overlay_opacity'.$suffix) ){
					$video_args['overlay_opacity'.$suffix] = $opacity;
				}

				if( $percent = mason_get_sub_field('video_block_overlay_percent'.$suffix) ){
					$video_args['percent'.$suffix] = $percent;
				}
				else{
					$video_args['percent'.$suffix] = "50";				
				}
				if( $color = mason_get_sub_field('video_block_overlay_color'.$suffix) ){
					$video_args['overlay_color'.$suffix] = $color;
				}
			}		
			$image = mason_get_sub_field('video_block_image'.$suffix);
			if( is_array( $image ) ){
				$video_args['bg_image_url'.$suffix] = $image['url'];
			}
			$mp4_file = mason_get_sub_field('video_video_file_mp4'.$suffix);
			if( is_array( $mp4_file ) ){
				//$video_args['width_class'.$suffix] . " video-video";
				$video_args['video_file_mp4'.$suffix] = $mp4_file['url'];
				$video_args['fallback_image_url'.$suffix] = $image['url'];
			}
			$video_args['youtube'.$suffix] = mason_get_sub_field('video_block_youtube'.$suffix);
			$bg_mp4_file = mason_get_sub_field('video_bg_video_file_mp4'.$suffix);
			if( is_array( $bg_mp4_file ) ){
				$video_args['bg_video_file_mp4'.$suffix] = $bg_mp4_file['url'];
			}
			$video_args['bg_youtube'.$suffix] = mason_get_sub_field('video_block_bg_youtube'.$suffix);
			$args = array_merge($video_args,$args);
		}
		$args['module_styles'] = [];
		if(get_sub_field('video_block_padding-top')){
			$args['module_styles']['padding-top'] = get_sub_field('video_block_padding-top');
		}
		if(get_sub_field('video_block_padding-bottom')){
			$args['module_styles']['padding-bottom'] = get_sub_field('video_block_padding-bottom');
		}

		if( is_array( $args ) ){
			return $args;
		}
	}
	function video_admin_enqueue($hook) {
		wp_register_style( 'video_wp_admin_css', get_template_directory_uri() . '/mason-modules/video/admin-style.css', false, '1.0.0' );
        wp_enqueue_style( 'video_wp_admin_css' );
    }
	add_action( 'admin_enqueue_scripts', 'video_admin_enqueue' );
?>