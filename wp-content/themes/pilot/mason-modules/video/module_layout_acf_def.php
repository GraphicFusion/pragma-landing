<?php
global $pilot;
// add module layout to flexible content
$use_halfwidth = 0;
function build_array($suffix = "", $name = "", $class = ""){
    $conditional_logic =  array(
            'field' => create_key('video','two_half_widths'),
            'operator' => '==',
            'value' => '0',
        );
    if('second' == $class){
        $conditional_logic = array(
            'field' => create_key('video','two_half_widths'),
            'operator' => '==',
            'value' => '1',
        );
    }
    $subfields = [
        array(
            'key' => create_key('video','space'.$suffix),
            'label' => $name,
            'name' => 'video_block_space'.$suffix,
            'type' => 'message',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '100',
				'class' => '',
				'id' => '',
			),
			'message' => '',
			'new_lines' => 'wpautop',
			'esc_html' => 0,
        ),
        array(
            'key' => create_key('video','bg_youtube'.$suffix),
            'label' => 'YoutTube Link ID' . $name,
            'name' => 'video_block_bg_youtube'.$suffix,
            'type' => 'text',
            'instructions' => 'Looks like "zprRZ2wFQD4"',
            'required' => 0,
            'wrapper' => array(
                'width' => '30',
                'class' => $class,
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
            'readonly' => 0,
            'disabled' => 0,
        ),        
        array(
            'key' => create_key('video','image'.$suffix),
            'label' => 'Background Image' . $name,
            'name' => 'video_block_image'.$suffix,
            'type' => 'image',
            'instructions' => '',
            'required' => 0,
            'wrapper' => array(
                'width' => '30%',
                'class' => $class,
                'id' => '',
            ),
            'return_format' => 'array',
            'preview_size' => 'thumbnail',
            'library' => 'all',
            'min_width' => '',
            'min_height' => '',
            'min_size' => '',
            'max_width' => '',
            'max_height' => '',
            'max_size' => '',
            'mime_types' => '',
        ),
/*
        array(
            'key' => create_key('video','subtitle'.$suffix),
            'label' => 'Subtitle' . $name,
            'name' => 'video_block_subtitle'.$suffix,
            'type' => 'text',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => array(
                array(
                    array(
                        'field' => create_key('video','use_content'.$suffix),
                        'operator' => '==',
                        'value' => '1',
                    ),
                    $conditional_logic
                ),
            ),
            'wrapper' => array(
                'width' => '',
                'class' => $class,
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
            'readonly' => 0,
            'disabled' => 0,
        ),
        */
		array(
            'key' => create_key('video','title'.$suffix),
            'label' => 'Title' . $name,
            'name' => 'video_block_title'.$suffix,
            'type' => 'text',
            'instructions' => '',
            'required' => 0,
            'wrapper' => array(
                'width' => 100,
                'class' => $class,
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
            'readonly' => 0,
            'disabled' => 0,
        ),
        array (
            'key' => create_key('video','content'.$suffix),
            'label' => 'Content' . $name,
            'name' => 'video_block_content'.$suffix,
            'type' => 'wysiwyg',
            'instructions' => '',
            'required' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => $class,
                'id' => '',
            ),
            'default_value' => '',
            'tabs' => 'all',
            'toolbar' => 'full',
            'video_upload' => 1,
        ),
        /*
        array(
            'key' => create_key('video','link'.$suffix),
            'label' => 'Button Link' . $name,
            'name' => 'video_block_link'.$suffix,
            'type' => 'link',
            'instructions' => '',
            'required' => 0,
            'wrapper' => array(
                'width' => '100',
                'class' => $class,
                'id' => '',
            ),
            'post_type' => '',
            'taxonomy' => '',
            'allow_null' => 0,
            'multiple' => 0,
            'return_format' => 'object',
            'ui' => 1,
        ),
        */
    ];
    return $subfields;
}
$subfields_first = build_array();
$subfields_second = [];
if( $use_halfwidth == 1){
	$subfields_second = build_array('_second',' - Second Card', 'second');
        array_unshift(
            $subfields,
            array(
                'key' => create_key('video','two_half_widths'),
                'label' => 'Block Width - One Full or Two Halves ',
                'name' => 'video_block_two_half_widths',
                'type' => 'true_false',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '100',
                    'class' => '',
                    'id' => '',
                ),
                'message' => '',
                'default_value' => 0,
                'ui' => 1,
                'ui_on_text' => 'Half Width',
                'ui_off_text' => 'Full Width',
            )
        );
}
$subfields = array_merge($subfields_first, $subfields_second);
$module_layout = array(
    'key' => create_key('video','block'),
    'name' => 'video_block',
    'label' => 'YouTube Video Block',
    'display' => 'block',
    'sub_fields' => $subfields,
    'min' => '',
    'max' => '',
);

?>