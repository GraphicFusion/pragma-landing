<?php
	function build_slideshow_layout(){
		$rows_arr = mason_get_sub_field('slideshow_block_rows');
		$args = [];
		$args['isHeader'] = mason_get_sub_field('slideshow_block_isHeader');
		$args['position'] = mason_get_sub_field('slideshow_block_left');
		$height = mason_get_sub_field('slideshow_block_height');
		$args['height'] = ($height ? $height : 99);
		if( is_array($rows_arr) ):
			$rows = array();
			foreach( $rows_arr as $row_arr){
				$rows[] = array(
					'title' => $row_arr['slideshow_block_title'],
					'content' => $row_arr['slideshow_block_content'],
					'subtitle' => $row_arr['slideshow_block_subtitle'],
					'image' => $row_arr['slideshow_block_image'],
				);
				
			}
			while ( mason_have_rows('slideshow_block_rows') ) : the_row();
				$rows[] = array(
					'title' => get_sub_field('slideshow_block_title'),
					'content' => get_sub_field('slideshow_block_content'),
					'subtitle' => get_sub_field('slideshow_block_subtitle'),
					'image' => get_sub_field('slideshow_block_image'),
				);
			endwhile;
			$args['rows'] = $rows;
		endif;
		$args['module_styles'] = [];
		if(get_sub_field('slideshow_block_padding-top')){
			$args['module_styles']['padding-top'] = get_sub_field('slideshow_block_padding-top');
		}
		if(get_sub_field('slideshow_block_padding-bottom')){
			$args['module_styles']['padding-bottom'] = get_sub_field('slideshow_block_padding-bottom');
		}

		return $args;
	}
function wp_durazo_enqueue_scripts() {
	wp_enqueue_script( 'slide_script', get_template_directory_uri() . "/mason-modules/slideshow/script.js", array('jquery'), null, true );
	//wp_enqueue_script( 'slick_script', get_template_directory_uri() . "/mason-modules/slideshow/slick.min.js" , array('jquery'), null, true);
	wp_enqueue_style( 'slick_style',"https://kenwheeler.github.io/slick/slick/slick-theme.css");
	wp_enqueue_style( 'slick_theme_style',"https://kenwheeler.github.io/slick/slick/slick.css");

	wp_enqueue_script( 'popup_script', get_template_directory_uri() . "/mason-modules/slideshow/html5lightbox.js" , array('jquery'), null, true);
	//wp_enqueue_style( 'popup_style',get_template_directory_uri() . "/mason-modules/slides/slick-lightbox.css");

}
add_action( 'wp_enqueue_scripts', 'wp_durazo_enqueue_scripts' );
add_action( 'wp_enqueue_scripts', 'wp_durazo_enqueue_scripts' );

?>