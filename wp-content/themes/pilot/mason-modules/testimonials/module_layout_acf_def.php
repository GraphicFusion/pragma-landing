<?php

	global $pilot;
	// add module layout to flexible content 
	$name = "testimonials";
	$module_layout = array (
		'key' => create_key($name, 'block'),
		'name' => 'testimonials_block',
		'label' => 'Testimonials Block',
		'display' => 'block',
		'sub_fields' => array (
			array (
				'key' => create_key($name,'rows'),
				'label' => 'Testimonials',
				'name' => 'testimonials_block_rows',
				'type' => 'repeater',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'collapsed' => '',
				'min' => '',
				'max' => '',
				'layout' => 'block',
				'button_label' => 'Add Row',
				'sub_fields' => array (

					array (
						'key' => create_key($name,'title'),
						'label' => 'Testimonial',
						'name' => 'testimonials_block_testimonial',
						'type' => 'wysiwyg',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
						'readonly' => 0,
						'disabled' => 0,
					),
					array (
						'key' => create_key($name,'attribution'),
						'label' => 'Attribution',
						'name' => 'testimonials_block_attribution',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
						'readonly' => 0,
						'disabled' => 0,
					),
				),
			),
		),
		'min' => '',
		'max' => '',
	);
?>