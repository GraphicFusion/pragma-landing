<?php
	$filename = dirname(__FILE__) . '/module_layout_acf_def.php';
	if( file_exists ( $filename )){
		require $filename;
	}
	function build_testimonials_layout(){
		$rows_arr = mason_get_sub_field('testimonials_block_rows');
		$args = [];
		if( is_array($rows_arr) ):
			$rows = array();
			foreach( $rows_arr as $row_arr){
				$rows[] = array(
					'testimonial' => $row_arr['testimonials_block_testimonial'],
					'attribution' => $row_arr['testimonials_block_attribution']
				);
				
			}
			while ( mason_have_rows('testimonials_block_rows') ) : the_row();
				$rows[] = array(
					'testimonial' => get_sub_field('testimonials_block_testimonial'),
					'attribution' => get_sub_field('testimonials_block_attribution')
				);
			endwhile;
			$args['rows'] = $rows;
		endif;
		return $args;
	}

?>