<?php
	global $pilot;
	// add module layout to flexible content 
	$module_layout = array (
		'key' => '569d9qwe87wt43t3231',
		'name' => 'location_block',
		'label' => 'Location Theme',
		'display' => 'block',
		'sub_fields' => array (
			array(
				'key' => 'field_5bdcd5da9cecd',
				'label' => 'Address',
				'name' => 'location_block_address',
				'type' => 'google_map',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'center_lat' => '36.1699',
				'center_lng' => '-115.1398',
				'zoom' => '',
				'height' => 300,
			),
        array(
            'key' => create_key('location','link'),
            'label' => 'Button Link',
            'name' => 'location_block_link',
            'type' => 'link',
            'instructions' => '',
            'required' => 0,
            'wrapper' => array(
                'width' => '40',
                'class' => '',
                'id' => '',
            ),
            'taxonomy' => '',
            'allow_null' => 0,
            'multiple' => 0,
            'return_format' => 'object',
            'ui' => 1,
        ),

		),
		'min' => '',
		'max' => '',
	);
?>