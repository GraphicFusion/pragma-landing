<?php global $pilot; ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div>
		<?php echo do_shortcode('[mason_build_blocks container=content]');?>
	</div>
</article>